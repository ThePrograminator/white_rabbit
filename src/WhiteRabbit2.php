<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $CoinArray = array(
            '1'   => 0,
            '2'   => 0,
            '5'   => 0,
            '10'  => 0,
            '20'  => 0,
            '50'  => 0,
            '100' => 0
        );

        if ($amount == 0)
        {
            return $CoinArray;
        }

        $coinSum = 0;
        $currentCoin = $this->findCurrentCoin($amount);
        $amountLeft = $amount;

        //If we haven't reached the amount yet, continue
        while ($coinSum != $amount)
        {
            if ($amountLeft - $currentCoin == 0)
            {
                //place new coin in array, and finish because we have reached the total amount
                $CoinArray[(String)$currentCoin] += 1;
                return $CoinArray;
            }
            else if ($amountLeft - $currentCoin > 0)
            {
                //place new coin in array, deduct what is left of the amount, increase the sum of coins used

                $CoinArray[(String)$currentCoin] =+ 1;

                $amountLeft = $amountLeft - $currentCoin;

                $coinSum = $coinSum + $currentCoin;
            }

            //Check if we need new type of coin
            $currentCoin = $this->findCurrentCoin($amountLeft);
        }
        return $CoinArray;
    }

    private function findCurrentCoin($amount)
    {
        //Find coin to use based upon the closest number equal to or above it's value
        switch (true) {
            case $amount == 1:
                return 1;
            case $amount >= 2 && $amount < 5:
                return 2;
            case $amount >= 5 && $amount < 10:
                return 5;
            case $amount >= 10 && $amount < 20:
                return 10;
            case $amount >= 20 && $amount < 50:
                return 20;
            case $amount >= 50 && $amount < 100:
                return 50;
            case $amount >= 100:
                return 100;
            default:
                return 0;
        }
    }
}