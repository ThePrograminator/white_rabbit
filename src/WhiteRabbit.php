<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $myFile = fopen($filePath, "r") or die("Unable to open file!");

        $string = file_get_contents($filePath);

        //count letters while ignoring case and placing them in an Associative Array
        $strArray = count_chars(strtolower($string),1);
        $iterateArray = $strArray;

        //remove any chars picked up which are not a letter
        foreach ($iterateArray as $key=>$value)
        {
            if (!preg_match("/^[a-z]$/", chr($key)))
            {
                unset($strArray[$key]);
            }
        }

        return $strArray;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $median = $this->calculateMedian($parsedFile);

        $closestValue = null;
        $closestKey = null;

        foreach ($parsedFile as $key => $value)
        {
            //figure out which value is closest to the median number and save the key - value
            if ($closestValue === null || abs($median - $closestValue) > abs($value - $median))
            {
                $closestValue = $value;
                $closestKey = $key;
            }
        }

        $occurrences = $closestValue;
        return chr($closestKey);
    }

    private function calculateMedian($array)
    {
        $tempArray = $array;

        sort($tempArray, SORT_NUMERIC);

        $amountOfLetters = count($tempArray);

        if($amountOfLetters % 2 == 0)
        {
            //even number of letters
            $median = $tempArray[$amountOfLetters / 2];
            return $median;
        }
        else
        {
            //uneven number of letters
            $median = (($tempArray[($amountOfLetters / 2)] + $tempArray[($amountOfLetters / 2) - 1]) / 2);
            return $median;
        }
    }
}